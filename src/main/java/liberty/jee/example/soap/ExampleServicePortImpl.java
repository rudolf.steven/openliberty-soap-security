package liberty.jee.example.soap;

import liberty.jee.example.auth.DefinedRoles;
import liberty.jee.example.servlet.SessionBean;
import liberty.jee.example.v1.Soap_002fV1_002fExampleService;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.soap.MTOM;

@MTOM
@WebService(
        serviceName = "soap/v1/ExampleService",
        portName = "soap/v1/ExampleServicePort",
        targetNamespace = "http://v1.example.jee.liberty/",
        wsdlLocation = "WEB-INF/wsdl/ExampleServiceV1.wsdl",
        endpointInterface = "liberty.jee.example.v1.Soap_002fV1_002fExampleService")
public class ExampleServicePortImpl implements Soap_002fV1_002fExampleService {

    @Inject
    SessionBean sessionBean;

    @Resource
    WebServiceContext webServiceContext;

    public String ping() {
        try {
            System.out.println("------");
            System.out.println("webServiceContext = " + webServiceContext);
            System.out.println("webServiceContext user = " + webServiceContext.getUserPrincipal());
            System.out.println("webServiceContext user in role = " + webServiceContext.isUserInRole(DefinedRoles.ROLE_TECHNICAL));
            System.out.println("------");

            final String ret = this.sessionBean.checkForTechnicalAccess();

            return ret;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
