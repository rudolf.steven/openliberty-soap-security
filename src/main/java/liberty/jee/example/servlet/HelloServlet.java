package liberty.jee.example.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/servlet")
public class HelloServlet extends HttpServlet {

    @Inject
    SessionBean sessionBean;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        StringBuilder builder = new StringBuilder();
        builder.append("Hello! How are you today?\n");
        builder.append("  Your principal is ").append(request.getUserPrincipal()).append("\n");
        builder.append("  You are routed to: ").append(request.getContextPath()).append("\n");
        try {
            builder.append(this.sessionBean.checkForRead());
        } catch (Exception e) {
            // do nothing.. there is simply not output in StringBuilder
        }
        try {
            builder.append(this.sessionBean.checkForTechnicalAccess());
        } catch (Exception e) {
            // do nothing.. there is simply not output in StringBuilder
        }

        response.getWriter().append(builder.toString());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}