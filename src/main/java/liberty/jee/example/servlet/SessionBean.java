package liberty.jee.example.servlet;

import liberty.jee.example.auth.DefinedRoles;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

@Stateless
public class SessionBean {

    @RolesAllowed(DefinedRoles.ROLE_READ)
    public String checkForRead() {
        return "  You are in group " + DefinedRoles.ROLE_READ;
    }

    @RolesAllowed(DefinedRoles.ROLE_TECHNICAL)
    public String checkForTechnicalAccess() {
        return "  You are in group " + DefinedRoles.ROLE_TECHNICAL;
    }
}
