package liberty.jee.example.auth;

/**
 * Alle Application Roles
 */
public final class DefinedRoles {
    public static final String ROLE_READ = "ROLE_READ";
    public static final String ROLE_TECHNICAL = "ROLE_TECHNICAL";
}
