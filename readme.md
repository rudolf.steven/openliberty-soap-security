Soap Security Example

This is a maven project using openliberty as JEE platform container for showing an example of providing a SOAP webservice.
This is to show how to provide a webservice using the JEE-8-feature and ws-security-feature of the apenliberty application server. Mainly the authentication and authorisation mechanism using a SOAP policy should be shown.

* to start the application you have to generate the webservice-stubs at first:
`mvn clean compile`

* and then start the application server: `mvn liberty:dev`